<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { //Se puede colocar cualquier ruta deseada
  //  return view('welcome'); //Vista ubicada en Recursos >> Vistas
//});

//Route::get('/empleados', 'EmpleadosController@index');

Route::resource('empleados', 'EmpleadosController');
